﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Runtime.CompilerServices;


[assembly: InternalsVisibleTo("XploRe.Json.Tests")]
[assembly: InternalsVisibleTo("XploRe.Net.JsonRpc")]
[assembly: InternalsVisibleTo("XploRe.Net.JsonRpc.Tests")]
