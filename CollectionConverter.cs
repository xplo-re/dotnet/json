﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using JetBrains.Annotations;
using Newtonsoft.Json;
using XploRe.Json.Internal;
using XploRe.Reflection;


namespace XploRe.Json
{

    /// <inheritdoc />
    /// <summary>
    ///     Converts a complex JSON object into .NET primitives. For arrays, the configured list collection type
    ///     <typeparamref name="TList" /> is used. Objects are deserialized into the configured dictionary type
    ///     <typeparamref name="TDictionary" />, storing each object property as a 
    ///     <see cref="T:System.Collections.Generic.KeyValuePair`2" />.
    /// </summary>
    /// <typeparam name="T">
    ///     Type used for items. If a deserialised value cannot be converted into <typeparamref name="T" />, an
    ///     exception is thrown. Can be used to enforce a certain data type, e.g. string, for all primitives. To retain
    ///     the JSON type of a primitive, use <see cref="T:System.Object" />.
    /// </typeparam>
    /// <typeparam name="TList">
    ///     List type that implements <see cref="T:System.Collections.Generic.ICollection`1" /> that is used for arrays. 
    ///     Must provide a parameterless constructor.
    /// </typeparam>
    /// <typeparam name="TDictionary">
    ///     Dictionary type that implements <see cref="T:System.Collections.Generic.IDictionary`2" /> that is used for 
    ///     objects whose properties are stored as <see cref="T:System.Collections.Generic.KeyValuePair`2" />s. Keys are 
    ///     always <see cref="T:System.String" />s.
    /// </typeparam>
    public class CollectionConverter<T, TList, TDictionary> : JsonConverter
        where TList : ICollection<T>, new()
        where TDictionary : IDictionary<string, T>, new()
    {

        /// <summary>
        ///     The <see cref="Type" /> used for collection items.
        /// </summary>
        public static Type ItemType => typeof(T);

        /// <summary>
        ///     The <see cref="Type" /> used for lists.
        /// </summary>
        public static Type ListType => typeof(TList);

        /// <summary>
        ///     The <see cref="Type" /> used for dictionaries.
        /// </summary>
        public static Type DictionaryType => typeof(TDictionary);

        /// <inheritdoc />
        public override bool CanConvert([NotNull] Type objectType)
        {
            if (objectType == null) {
                throw new ArgumentNullException(nameof(objectType));
            }

            if (objectType == ItemType || objectType == ListType || objectType == DictionaryType) {
                return true;
            }

            var typeInfo = objectType.GetTypeInfo();

            if (null == typeInfo) {
                return false;
            }

            if (typeInfo.IsGenericType) {
                var genericType = typeInfo.GetGenericTypeDefinition();

                if (genericType == typeof(IDictionary<,>)) {
                    // We have a dictionary. Check, whether the type arguments are assignable.
                    var argumentTypes = typeInfo.GenericTypeArguments;

                    if (argumentTypes == null || argumentTypes.Length != 2) {
                        return false;
                    }

                    var keyType = argumentTypes[0];
                    var valueType = argumentTypes[1];

                    if (keyType.GetTypeInfo()?.IsAssignableFrom(typeof(string)) != true) {
                        return false;
                    }

                    if (valueType.GetTypeInfo()?.IsAssignableFrom(typeof(T)) != true) {
                        return false;
                    }

                    return true;
                }

                if (genericType == typeof(ICollection<>)) {
                    // We have a list. Check, whether the type argument is assignable.
                    var argumentTypes = typeInfo.GenericTypeArguments;

                    if (argumentTypes == null || argumentTypes.Length != 1) {
                        return false;
                    }

                    var valueType = argumentTypes[0];

                    if (valueType.GetTypeInfo()?.IsAssignableFrom(typeof(T)) == true) {
                        return true;
                    }
                }
            }

            // Primitive types are also supported, but are handled the same way as the standard converter.
            return false;
        }

        /// <inheritdoc />
        public override bool CanRead => true;

        /// <inheritdoc />
        [CanBeNull]
        public override object ReadJson(
            [NotNull] JsonReader reader,
            [NotNull] Type objectType,
            object existingValue,
            [NotNull] JsonSerializer serializer)
        {
            if (reader == null) {
                throw new ArgumentNullException(nameof(reader));
            }

            if (objectType == null) {
                throw new ArgumentNullException(nameof(objectType));
            }

            if (serializer == null) {
                throw new ArgumentNullException(nameof(serializer));
            }

            return ReadValue(reader, serializer);
        }

        /// <summary>
        ///     Called recursively to read and deserialise the current value. 
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader" /> instance to read from.</param>
        /// <param name="serializer">The <see cref="JsonSerializer" /> used for deserialisation.</param>
        /// <returns>Deserialised object.</returns>
        /// <exception cref="JsonSerializationException">
        ///     Deserialisation failed. The message contains further details on the cause and stream position.
        /// </exception>
        [CanBeNull]
        protected virtual object ReadValue([NotNull] JsonReader reader, [NotNull] JsonSerializer serializer)
        {
            if (!reader.MoveToContent()) {
                throw JsonHelper.CreateSerializationException(
                    reader,
                    "Unexpected end when reading value."
                );
            }

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (reader.TokenType) {
            case JsonToken.Boolean:
            case JsonToken.Date:
            case JsonToken.Float:
            case JsonToken.Integer:
            case JsonToken.Null:
            case JsonToken.String:
                // Primitive type.
                return reader.Value;

            case JsonToken.Bytes:
                return reader.ReadAsBytes();

            case JsonToken.StartArray:
                return ReadArray(reader, serializer);

            case JsonToken.StartObject:
                return ReadObject(reader, serializer);

            case JsonToken.StartConstructor:
                throw JsonHelper.CreateSerializationException(
                    reader,
                    $"JavaScript constructors are not supported by the type {GetType()}."
                );

            default:
                throw JsonHelper.CreateSerializationException(
                    reader,
                    $"Unsupported token {reader.TokenType} encountered."
                );
            }
        }

        /// <summary>
        ///     Reads each child entry of the current array token into a <typeparamref name="TList" />.
        /// </summary>
        /// <returns>
        ///     New <typeparamref name="TList" /> instance holding all children of the current array token.
        /// </returns>
        private TList ReadArray([NotNull] JsonReader reader, [NotNull] JsonSerializer serializer)
        {
            var list = new TList();

            while (reader.ReadAndMoveToContent()) {
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (reader.TokenType) {
                case JsonToken.EndArray:
                    return list;

                default:
                    var item = ReadValue(reader, serializer);
                    list.Add((T) item);
                    break;
                }
            }

            throw JsonHelper.CreateSerializationException(
                reader,
                "Unexpected end when reading array items."
            );
        }

        /// <summary>
        ///     Reads each child entry of the current object token into a <typeparamref name="TDictionary" />.
        /// </summary>
        /// <returns>
        ///     New <typeparamref name="TDictionary" /> instance with all properties found at the current object token
        ///     stored as <see cref="KeyValuePair{TKey,TValue}" /> instances.
        /// </returns>
        private TDictionary ReadObject([NotNull] JsonReader reader, [NotNull] JsonSerializer serializer)
        {
            var dictionary = new TDictionary();

            while (reader.ReadAndMoveToContent()) {
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (reader.TokenType) {
                case JsonToken.EndObject:
                    return dictionary;

                case JsonToken.PropertyName:
                    var propertyName = reader.Value?.ToString();
                    Debug.Assert(propertyName != null, $"{nameof(propertyName)} != null");

                    if (!reader.ReadAndMoveToContent()) {
                        throw JsonHelper.CreateSerializationException(
                            reader,
                            "Unexpected end when reading object property."
                        );
                    }

                    var value = ReadValue(reader, serializer);

                    dictionary.Add(propertyName, (T) value);
                    break;
                }
            }

            throw JsonHelper.CreateSerializationException(
                reader,
                "Unexpected end when reading object properties."
            );
        }

        /// <inheritdoc />
        public override bool CanWrite => false;

        /// <inheritdoc />
        public override void WriteJson(
            [NotNull] JsonWriter writer,
            object value,
            [NotNull] JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

    }

    /// <inheritdoc />
    /// <summary>
    ///     Converts a complex JSON object into .NET primitives, using <see cref="T:System.Collections.Generic.List`1" /> 
    ///     for arrays and <see cref="T:System.Collections.Generic.Dictionary`2" /> for objects whose properties are 
    ///     stored as <see cref="T:System.Collections.Generic.KeyValuePair`2" />s. Keys are always
    ///     <see cref="T:System.String" />s.
    /// </summary>
    /// <typeparam name="T">
    ///     Type used for items. If a deserialised value cannot be converted into <typeparamref name="T" />, an
    ///     exception is thrown. Can be used to enforce a certain data type, e.g. string, for all primitives. To retain
    ///     the JSON type of a primitive, use <see cref="T:System.Object" />.
    /// </typeparam>
    public class CollectionConverter<T> : CollectionConverter<T, List<T>, Dictionary<string, T>>
    {

    }

    /// <inheritdoc />
    /// <summary>
    ///     Converts a complex JSON object into .NET primitives, using <see cref="T:System.Collections.Generic.List`1" /> 
    ///     for arrays and <see cref="T:System.Collections.Generic.Dictionary`2" /> for objects whose properties are 
    ///     stored as <see cref="T:System.Collections.Generic.KeyValuePair`2" />s. Keys are always 
    ///     <see cref="T:System.String" />s.
    /// </summary>
    public class CollectionConverter : CollectionConverter<object>
    {

    }

}
