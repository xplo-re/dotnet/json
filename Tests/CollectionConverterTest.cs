﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using FluentAssertions;
using Newtonsoft.Json;
using Xunit;


// ReSharper disable PossibleNullReferenceException

namespace XploRe.Json.Tests
{

    public class CollectionConverterTest
    {

        public class Generator
        {

            public static IEnumerable<object[]> Primitives
            {
                get {
                    yield return new object[] { @"null", null };
                    yield return new object[] { @"0", 0 };
                    yield return new object[] { @"42", 42 };
                    yield return new object[] { @"-42", -42 };
                    yield return new object[] { @"0.5", 0.5 };
                    yield return new object[] { @"-0.42", -0.42 };
                    yield return new object[] { @"010", Convert.ToInt32("010", 8) };
                    yield return new object[] { @"""text""", "text" };
                    yield return new object[] { @"true", true };
                    yield return new object[] { @"false", false };
                }
            }

            public static IEnumerable<object[]> Lists
            {
                get {
                    var empty = new List<object>();

                    yield return new object[] { @"[]", empty };
                    yield return new object[] { @"[ 1, 2 ]", new List<object> { 1, 2 } };
                    yield return new object[] { @"[ 1, ""text"" ]", new List<object> { 1, "text" } };
                    yield return new object[] { @"[ [], [] ]", new List<object> { empty, empty } };
                }
            }

            public static IEnumerable<object[]> Dictionaries
            {
                get {
                    var empty = new Dictionary<string, object>();

                    yield return new object[] {
                        @"{}",
                        empty
                    };

                    yield return new object[] {
                        @"{ ""a"": 1, ""b"": 2 }",
                        new Dictionary<string, object> {
                            { "a", 1 },
                            { "b", 2 }
                        }
                    };

                    yield return new object[] {
                        @"{ ""a"": {}, ""b"": {} }",
                        new Dictionary<string, object> {
                            { "a", empty },
                            { "b", empty }
                        }
                    };

                    yield return new object[] {
                        @"{ ""a"": 42, ""b"": [ 1, 2, 3 ] }",
                        new Dictionary<string, object> {
                            { "a", 42 },
                            { "b", new List<object> { 1, 2, 3 } }
                        }
                    };

                    yield return new object[] {
                        @"{ ""a"": 42, ""b"": [ 1, 2, 3 ], ""c"": { ""value"": [ 42, 24 ] } }",
                        new Dictionary<string, object> {
                            { "a", 42 },
                            { "b", new List<object> { 1, 2, 3 } }, {
                                "c",
                                new Dictionary<string, object> {
                                    { "value", new List<object> { 42, 24 } }
                                }
                            }
                        }
                    };
                }
            }

        }

        [Theory]
        [MemberData(nameof(Generator.Primitives), MemberType = typeof(Generator))]
        [MemberData(nameof(Generator.Lists), MemberType = typeof(Generator))]
        [MemberData(nameof(Generator.Dictionaries), MemberType = typeof(Generator))]
        public void JsonSerializeTest(string json, object expected)
        {
            /*
            var jsonSettings = new JsonSerializerSettings {
                Converters = { new CollectionConverter() },
            };
            */

            var deserialized = JsonConvert.DeserializeObject<object>(json, new CollectionConverter());
            deserialized.ShouldBeEquivalentTo(expected);
        }

    }

}
