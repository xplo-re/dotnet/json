﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Diagnostics;
using JetBrains.Annotations;
using Newtonsoft.Json;


namespace XploRe.Json.Internal
{

    internal static class JsonHelper
    {

        #region JsonSerializationException

        // Based on original implementation for Newtonsoft.Json,
        // cf. https://github.com/JamesNK/Newtonsoft.Json/blob/master/Src/Newtonsoft.Json/JsonSerializationException.cs

        [NotNull]
        internal static JsonSerializationException CreateSerializationException(
            [NotNull] JsonReader reader,
            [CanBeNull] string message)
            => CreateSerializationException(reader, message, null);

        [NotNull]
        internal static JsonSerializationException CreateSerializationException(
            [NotNull] JsonReader reader,
            [CanBeNull] string message,
            [CanBeNull] Exception innerException)
        {
            if (reader == null) {
                throw new ArgumentNullException(nameof(reader));
            }

            return CreateSerializationException(reader as IJsonLineInfo, reader.Path, message, innerException);
        }

        [NotNull]
        internal static JsonSerializationException CreateSerializationException(
            [CanBeNull] IJsonLineInfo lineInfo,
            [CanBeNull] string path,
            [CanBeNull] string message,
            [CanBeNull] Exception innerException)
            => CreateSerializationException(
                lineInfo, path, message, innerException, (msg, e) => new JsonSerializationException(msg, e));

        [NotNull]
        internal static T CreateSerializationException<T>(
            [NotNull] JsonReader reader,
            [CanBeNull] string message,
            [NotNull] Func<string, Exception, T> constructor)
            where T : Exception
            => CreateSerializationException(reader, message, null, constructor);

        [NotNull]
        internal static T CreateSerializationException<T>(
            [NotNull] JsonReader reader,
            [CanBeNull] string message,
            [CanBeNull] Exception innerException,
            [NotNull] Func<string, Exception, T> constructor)
            where T : Exception
        {
            if (reader == null) {
                throw new ArgumentNullException(nameof(reader));
            }

            return CreateSerializationException(
                reader as IJsonLineInfo, reader.Path, message, innerException, constructor
            );
        }

        [NotNull]
        internal static T CreateSerializationException<T>(
            [CanBeNull] IJsonLineInfo lineInfo,
            [CanBeNull] string path,
            [CanBeNull] string message,
            [CanBeNull] Exception innerException,
            [NotNull] Func<string, Exception, T> constructor)
            where T : Exception
        {
            if (constructor == null) {
                throw new ArgumentNullException(nameof(constructor));
            }

            var result = constructor(
                message.AppendLineInfo(lineInfo, path ?? "<unknown>"),
                innerException
            );

            Debug.Assert(result != null, $"{nameof(result)} != null");
            return result;
        }

        #endregion

    }

}
