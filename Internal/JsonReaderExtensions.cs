﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using JetBrains.Annotations;
using Newtonsoft.Json;


namespace XploRe.Json.Internal
{

    internal static class JsonReaderExtensions
    {

        // Based on original implementation for Newtonsoft.Json,
        // cf. https://github.com/JamesNK/Newtonsoft.Json/blob/master/Src/Newtonsoft.Json/JsonReader.cs

        /// <summary>
        ///     Moves the internal position of this <see cref="JsonReader" /> instance to the first token that is not a 
        ///     <see cref="JsonToken.None" /> or <see cref="JsonToken.Comment" />.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader" /> instance.</param>
        /// <returns><c>true</c> on success, <c>false</c> if no other token could be read.</returns>
        internal static bool MoveToContent([NotNull] this JsonReader reader)
        {
            var tokenType = reader.TokenType;

            while (tokenType == JsonToken.None || tokenType == JsonToken.Comment) {
                if (!reader.Read()) {
                    return false;
                }

                tokenType = reader.TokenType;
            }

            return true;
        }

        /// <summary>
        ///     Reads the next token and moves the internal position according to <see cref="MoveToContent" />.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader" /> instance.</param>
        /// <returns><c>true</c> on success, <c>false</c> if no other token could be read.</returns>
        internal static bool ReadAndMoveToContent([NotNull] this JsonReader reader)
        {
            if (!reader.Read()) {
                return false;
            }

            return reader.MoveToContent();
        }

    }

}
