﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Globalization;
using JetBrains.Annotations;
using Newtonsoft.Json;
using XploRe.Diagnostics;


namespace XploRe.Json.Internal
{

    internal static class StringExtensions
    {

        /// <summary>
        ///     Appends JSON line information to a given message.
        /// </summary>
        /// <param name="message">The message to extend with line information.</param>
        /// <param name="lineInfo">The <see cref="IJsonLineInfo" /> instance to get line information from.</param>
        /// <param name="path">The path of the file corresponding file.</param>
        /// <returns>The provided <paramref name="message" /> extended with line information.</returns>
        internal static string AppendLineInfo(
            [CanBeNull] this string message,
            [CanBeNull] IJsonLineInfo lineInfo,
            [NotNull] string path)
        {
            var pathInfo = $"Path '{path}'";

            if (lineInfo != null && lineInfo.HasLineInfo()) {
                pathInfo += string.Format(
                    CultureInfo.InvariantCulture,
                    ", line {0}, position {1}",
                    lineInfo.LineNumber, lineInfo.LinePosition
                );
            }

            pathInfo += ".";

            return message.AppendDiagnosticsSentence(pathInfo);
        }

    }

}
